# Loop Closure Detection on Point Clouds - 3D Vision

## Introduction

[PointNetVLAD](https://arxiv.org/abs/1804.03492) and [LPD-Net](https://arxiv.org/abs/1804.03492) are deep networks that addresses the problem of large-scale place recognition through retrival from raw 3D point clouds. 
This is an PyTorch implementation of the LPD-Net based the original work of PointNetVLAD [here](https://github.com/mikacuy/pointnetvlad)

## Benchmark Datasets
The benchmark datasets introduced in the original work can be downloaded [here](https://drive.google.com/open?id=1H9Ep76l8KkUpwILY-13owsEMbVCYTmyx). Those are from the open-source [Oxford RobotCar](https://robotcar-dataset.robots.ox.ac.uk/) for large-scale place recognition.
The additional benchmark datasets which are also used in our work can be downloaded [here](https://drive.google.com/drive/folders/1d4YFA3bQf9Z1IRvImW4tukCTziVQC2Mb). Those are from the open-source [nuScenes](https://www.nuscenes.org/) for large-scale autonomous driving.

## Project Code
Code was tested using Python 3 and PyTorch 1.4 with CUDA 10.