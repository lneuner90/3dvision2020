# library/module dependencies
import argparse
import os
import sys
import numpy as np
import torch
import torch.nn as nn
from sklearn.neighbors import KDTree, NearestNeighbors
from tensorboardX import SummaryWriter
from enum import Enum
# project related modules
import models.PointNetVlad as PNV
import models.LPDNet as LPD
import loss as PNV_loss
import utils.loading_pointclouds as util_pcl

# =================================
# ===== parameter definitions =====
# =================================

#params from pointnetvlad
parser = argparse.ArgumentParser()
parser.add_argument('--positives_per_query', type=int, default=2,help='Number of potential positives in each training tuple [default: 2]')
parser.add_argument('--negatives_per_query', type=int, default=18,help='Number of definite negatives in each training tuple [default: 18]')
parser.add_argument('--max_epoch', type=int, default=20,help='Epoch to run [default: 20]')
parser.add_argument('--batch_num_queries', type=int, default=2, help='Batch Size during training [default: 2]')
parser.add_argument('--learning_rate', type=float, default=0.00005, help='Initial learning rate [default: 0.00005]')
parser.add_argument('--momentum', type=float, default=0.9, help='Initial learning rate [default: 0.9]')
parser.add_argument('--optimizer', default='adam', help='adam or momentum [default: adam]')
parser.add_argument('--decay_step', type=int, default=200000, help='Decay step for lr decay [default: 200000]')
parser.add_argument('--decay_rate', type=float, default=0.7, help='Decay rate for lr decay [default: 0.7]')
parser.add_argument('--margin_1', type=float, default=0.5, help='Margin for hinge loss [default: 0.5]')
parser.add_argument('--margin_2', type=float, default=0.2, help='Margin for hinge loss [default: 0.2]')

# params from pointnetvlad_pytorch
#parser.add_argument('--learning_rate', type=float, default=0.000005, help='Initial learning rate [default: 0.000005]')
parser.add_argument('--loss_function', default='quadruplet', choices=['triplet', 'quadruplet'], help='triplet or quadruplet [default: quadruplet]')
parser.add_argument('--loss_not_lazy', action='store_false',help='If present, do not use lazy variant of loss')
parser.add_argument('--loss_ignore_zero_batch', action='store_true',help='If present, mean only batches with loss > 0.0')
parser.add_argument('--triplet_use_best_positives', action='store_true',help='If present, use best positives, otherwise use hardest positives')
FLAGS = parser.parse_args()

# module parameters training (derived from argparse)
BATCH_NUM_QUERIES = 2#FLAGS.batch_num_queries
NUM_POINTS = 4096
TRAIN_POSITIVES_PER_QUERY = FLAGS.positives_per_query
TRAIN_NEGATIVES_PER_QUERY = FLAGS.negatives_per_query
MAX_EPOCH = 10 #FLAGS.max_epoch
BASE_LEARNING_RATE = FLAGS.learning_rate
MOMENTUM = FLAGS.momentum
OPTIMIZER = FLAGS.optimizer
DECAY_STEP = FLAGS.decay_step
DECAY_RATE = FLAGS.decay_rate
MARGIN_1 = FLAGS.margin_1
MARGIN_2 = FLAGS.margin_2
FEATURE_OUTPUT_DIM = 256

LOSS_FUNCTION = FLAGS.loss_function
LOSS_LAZY = True
TRIPLET_USE_BEST_POSITIVES = True
LOSS_IGNORE_ZERO_BATCH = False

RESUME = False

class ModelType(Enum):
    POINTNET_VLAD = 1
    LPD_NET       = 2

MODEL_TYPE = ModelType.LPD_NET

# file handling for training
SOURCE_DIR  = '/home/src/3DVisionETH/source/'
#SOURCE_DIR  = '/home/neur/private/3DVision/source/'
DATA_DIR    = '/home/data/3DVisionETH/'
#DATA_DIR    = '/home/neur/private/3DVision/'

TRAIN_FILE_PKL = [ DATA_DIR + 'queries/training_queries_baseline.pickle',
                   DATA_DIR + 'queries/training_queries_baseline_LPD.pickle']
TEST_FILE_PKL  = [ DATA_DIR + 'queries/test_queries_baseline.pickle',
                   DATA_DIR + 'queries/test_queries_baseline_LPD.pickle']
TRAINING_QUERIES = dict()
TEST_QUERIES     = dict()

DATASETS = DATA_DIR + 'benchmark_datasets'

MODEL_FILENAME = 'model_LPD_firstTry.pth.tar'

LOG_DIR = SOURCE_DIR + 'log/'
if not os.path.exists(LOG_DIR):
    os.mkdir(LOG_DIR)

BN_INIT_DECAY = 0.5
BN_DECAY_DECAY_RATE = 0.5
BN_DECAY_DECAY_STEP = float(DECAY_STEP)
BN_DECAY_CLIP = 0.99

HARD_NEGATIVES = dict()
TRAINING_LATENT_VECTORS = list()

# =================================
# ===== function definitions ======
# =================================
def get_bn_decay(batch):
    bn_momentum = BN_INIT_DECAY * \
        (BN_DECAY_DECAY_RATE **
         (batch * BATCH_NUM_QUERIES // BN_DECAY_DECAY_STEP))
    return min(BN_DECAY_CLIP, 1 - bn_momentum)

# learning rate halfed every 5 epoch
def get_learning_rate(epoch):
    learning_rate = BASE_LEARNING_RATE * ((0.9) ** (epoch // 5))
    learning_rate = max(learning_rate, 0.00001)  # CLIP THE LEARNING RATE!
    return learning_rate

def train(device):
    # from TensorboardX -> Generate a write-object to add summaries and events for visualization
    train_writer = SummaryWriter(os.path.join(LOG_DIR, 'train'))

    # --- Define network, loss function and optimizer ---
    if MODEL_TYPE == ModelType.LPD_NET:
        model = LPD.LPDNet(num_points=NUM_POINTS, output_dim=FEATURE_OUTPUT_DIM, use_neighborhood_aggr=False)
    else:
        model = PNV.PointNetVlad(global_feat=True, feature_transform=True,max_pool=False, output_dim=FEATURE_OUTPUT_DIM, num_points=NUM_POINTS)
    model = model.to(device)    # make sure the model is moved to the GPU

    # define loss-function
    if LOSS_FUNCTION == 'quadruplet':
        loss_function = PNV_loss.quadruplet_loss
    else:
        loss_function = PNV_loss.triplet_loss_wrapper
    print("Loss function: {0}".format(loss_function.__name__))

    # define optimizer
    learning_rate = get_learning_rate(0)   
    if OPTIMIZER == 'momentum':
        optimizer = torch.optim.SGD(model.parameters(), learning_rate, momentum=MOMENTUM)
    else:
        optimizer = torch.optim.Adam(model.parameters(), learning_rate)
    print("Optimizer: {0}".format(optimizer.__module__))

    # Per default: the training is performed with the quadruplet-loss and the ADAM-Optimizer using a lr = 5e-05

    # --- Load previous state of training or restart training ---
    if RESUME:
        fileToResume = DATA_DIR + MODEL_FILENAME
        print("Load previous training state: {0} ".format(fileToResume))
        checkpoint = torch.load(fileToResume)

        # TODO: still missing to define also the processed batch within an epoch before this effectivly can work
        starting_epoch = checkpoint['epoch']
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
    else:
        print("Restart training...")
        starting_epoch = 0

    for epoch in range(starting_epoch, MAX_EPOCH):
        print("***** RUN EPOCH {0} *****".format(epoch))
        train_one_epoch(device, model, optimizer, train_writer, loss_function, epoch)

def train_one_epoch(device, model, optimizer, train_writer, loss_function, epoch):
    global HARD_NEGATIVES, TRAINING_LATENT_VECTORS

    sampled_neg = 4000
    # number of hard negatives in the training tuple
    # which are taken from the sampled negatives
    num_to_take = 10

    # Shuffle train files
    train_file_idxs = np.arange(0, len(TRAINING_QUERIES.keys()))
    np.random.shuffle(train_file_idxs)
    
    for i in range(len(train_file_idxs)//BATCH_NUM_QUERIES):
        # Iterate through training quries in batches of size BATCH_NUM_QUERIES --> batch_keys = in this iteration processed queries (all queries are orginized via increasing keys in TRAINING_QUERIES)

        # ---- The tuple generation stems from the original PointNetVLAD impl. ---- 
        batch_keys = train_file_idxs[i * BATCH_NUM_QUERIES:(i+1)*BATCH_NUM_QUERIES]
        q_tuples = []

        faulty_tuple = False
        no_other_neg = False
        for j in range(BATCH_NUM_QUERIES):
            if (len(TRAINING_QUERIES[batch_keys[j]]["positives"]) < TRAIN_POSITIVES_PER_QUERY):
                faulty_tuple = True
                break

            # no cached feature vectors
            if (len(TRAINING_LATENT_VECTORS) == 0):
                q_tuples.append(util_pcl.get_query_tuple(DATASETS, TRAINING_QUERIES[batch_keys[j]], TRAIN_POSITIVES_PER_QUERY, TRAIN_NEGATIVES_PER_QUERY, TRAINING_QUERIES, hard_neg=[], other_neg=True))
                # q_tuples.append(get_rotated_tuple(TRAINING_QUERIES[batch_keys[j]],POSITIVES_PER_QUERY,NEGATIVES_PER_QUERY, TRAINING_QUERIES, hard_neg=[], other_neg=True))
                # q_tuples.append(get_jittered_tuple(TRAINING_QUERIES[batch_keys[j]],POSITIVES_PER_QUERY,NEGATIVES_PER_QUERY, TRAINING_QUERIES, hard_neg=[], other_neg=True))

            elif (len(HARD_NEGATIVES.keys()) == 0):
                query = get_feature_representation(TRAINING_QUERIES[batch_keys[j]]['query'], device, model)
                np.random.shuffle(TRAINING_QUERIES[batch_keys[j]]['negatives'])
                negatives = TRAINING_QUERIES[batch_keys[j]]['negatives'][0:sampled_neg]
                hard_negs = get_random_hard_negatives(query, negatives, num_to_take)
                print(hard_negs)
                q_tuples.append(util_pcl.get_query_tuple(DATASETS, TRAINING_QUERIES[batch_keys[j]], TRAIN_POSITIVES_PER_QUERY, TRAIN_NEGATIVES_PER_QUERY,TRAINING_QUERIES, hard_negs, other_neg=True))
                # q_tuples.append(get_rotated_tuple(TRAINING_QUERIES[batch_keys[j]],POSITIVES_PER_QUERY,NEGATIVES_PER_QUERY, TRAINING_QUERIES, hard_negs, other_neg=True))
                # q_tuples.append(get_jittered_tuple(TRAINING_QUERIES[batch_keys[j]],POSITIVES_PER_QUERY,NEGATIVES_PER_QUERY, TRAINING_QUERIES, hard_negs, other_neg=True))
            else:
                query = get_feature_representation(TRAINING_QUERIES[batch_keys[j]]['query'], device, model)
                np.random.shuffle(TRAINING_QUERIES[batch_keys[j]]['negatives'])
                negatives = TRAINING_QUERIES[batch_keys[j]]['negatives'][0:sampled_neg]
                hard_negs = get_random_hard_negatives(query, negatives, num_to_take)
                hard_negs = list(set().union(HARD_NEGATIVES[batch_keys[j]], hard_negs))
                print('hard', hard_negs)
                q_tuples.append(util_pcl.get_query_tuple(DATASETS, TRAINING_QUERIES[batch_keys[j]], TRAIN_POSITIVES_PER_QUERY, TRAIN_NEGATIVES_PER_QUERY,TRAINING_QUERIES, hard_negs, other_neg=True))
                # q_tuples.append(get_rotated_tuple(TRAINING_QUERIES[batch_keys[j]],POSITIVES_PER_QUERY,NEGATIVES_PER_QUERY, TRAINING_QUERIES, hard_negs, other_neg=True))
                # q_tuples.append(get_jittered_tuple(TRAINING_QUERIES[batch_keys[j]],POSITIVES_PER_QUERY,NEGATIVES_PER_QUERY, TRAINING_QUERIES, hard_negs, other_neg=True))

            if (q_tuples[j][3].shape[0] != NUM_POINTS):
                no_other_neg = True
                break

        if(faulty_tuple):
            print("---- Batch {0}: Queries {1} -- FAULTY TUPLE ----".format(i, batch_keys))
            continue

        if(no_other_neg):
            print("---- Batch {0}: Queries {1} -- NO OTHER NEG ----".format(i, batch_keys))
            continue

        queries = []
        positives = []
        negatives = []
        other_neg = []
        for k in range(len(q_tuples)):
            queries.append(q_tuples[k][0])
            positives.append(q_tuples[k][1])
            negatives.append(q_tuples[k][2])
            other_neg.append(q_tuples[k][3])

        queries = np.array(queries, dtype=np.float32)
        queries = np.expand_dims(queries, axis=1)
        other_neg = np.array(other_neg, dtype=np.float32)
        other_neg = np.expand_dims(other_neg, axis=1)
        positives = np.array(positives, dtype=np.float32)
        negatives = np.array(negatives, dtype=np.float32)
        if (len(queries.shape) != 4):
            print("---- Batch {0}: Queries {1} -- FAULTY QUERY ----".format(i, batch_keys))
            continue
        # --- End of tuple generation ---
        
        # ---- Train the model for current patch ---- 
        model.train()   # Set the module in training mode - only affects certain modules

        # Zero gradient buffer before completing the backward pass
        optimizer.zero_grad()

        output_queries, output_positives, output_negatives, output_other_neg = run_model(device, model, queries, positives, negatives, other_neg)
        loss = loss_function(output_queries, output_positives, output_negatives, output_other_neg, MARGIN_1, MARGIN_2, use_min=TRIPLET_USE_BEST_POSITIVES, lazy=LOSS_LAZY, ignore_zero_loss=LOSS_IGNORE_ZERO_BATCH)
        
        # use autograd to complete backward-pass (compute gradient w.r.t loss) and take optimization step 
        loss.backward()
        optimizer.step()

        print("---- Batch {0}: Queries {1} -- loss {2} ----".format(i, batch_keys, loss))

        # EVALLLL
        if (epoch > 5 and i % (1400 // BATCH_NUM_QUERIES) == 29):
            TRAINING_LATENT_VECTORS = get_latent_vectors(device, model, TRAINING_QUERIES)
            print("Updated cached feature vectors")

        if (i % (6000 // BATCH_NUM_QUERIES) == 101):
            #if isinstance(model, nn.DataParallel):
            #    model_to_save = model.module
            #else:
            #    model_to_save = model

            # This stores a general checkpoint for either inference and/or resuming the training
            # (see https://pytorch.org/tutorials/beginner/saving_loading_models.html)
            path_model = DATA_DIR + MODEL_FILENAME
            torch.save({
                'epoch': epoch,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict(),
            }, path_model)
            print("A general checkpoint of the model saved to", path_model)


def get_feature_representation(filename, device, model):
    model.eval()
    queries = util_pcl.load_pc_files(DATASETS, [filename])
    queries = np.expand_dims(queries, axis=1)
    # if(BATCH_NUM_QUERIES-1>0):
    #    fake_queries=np.zeros((BATCH_NUM_QUERIES-1,1,NUM_POINTS,3))
    #    q=np.vstack((queries,fake_queries))
    # else:
    #    q=queries
    with torch.no_grad():
        q = torch.from_numpy(queries).float()
        q = q.to(device)
        output = model(q)
    output = output.detach().cpu().numpy()
    output = np.squeeze(output)
    model.train()
    return output


def get_random_hard_negatives(query_vec, random_negs, num_to_take):
    global TRAINING_LATENT_VECTORS

    latent_vecs = []
    for j in range(len(random_negs)):
        latent_vecs.append(TRAINING_LATENT_VECTORS[random_negs[j]])

    latent_vecs = np.array(latent_vecs)
    nbrs = KDTree(latent_vecs)
    distances, indices = nbrs.query(np.array([query_vec]), k=num_to_take)
    hard_negs = np.squeeze(np.array(random_negs)[indices[0]])
    hard_negs = hard_negs.tolist()
    return hard_negs


def get_latent_vectors(device, model, dict_to_process):
    train_file_idxs = np.arange(0, len(dict_to_process.keys()))

    batch_num = BATCH_NUM_QUERIES * \
        (1 + TRAIN_POSITIVES_PER_QUERY + TRAIN_NEGATIVES_PER_QUERY + 1)
    q_output = []

    model.eval()

    for q_index in range(len(train_file_idxs)//batch_num):
        file_indices = train_file_idxs[q_index *
                                       batch_num:(q_index+1)*(batch_num)]
        file_names = []
        for index in file_indices:
            file_names.append(dict_to_process[index]["query"])
        queries = util_pcl.load_pc_files(DATASETS, file_names)

        feed_tensor = torch.from_numpy(queries).float()
        feed_tensor = feed_tensor.unsqueeze(1)
        feed_tensor = feed_tensor.to(device)
        with torch.no_grad():
            out = model(feed_tensor)

        out = out.detach().cpu().numpy()
        out = np.squeeze(out)

        q_output.append(out)

    q_output = np.array(q_output)
    if(len(q_output) != 0):
        q_output = q_output.reshape(-1, q_output.shape[-1])

    # handle edge case
    for q_index in range((len(train_file_idxs) // batch_num * batch_num), len(dict_to_process.keys())):
        index = train_file_idxs[q_index]
        queries = util_pcl.load_pc_files(DATASETS, [dict_to_process[index]["query"]])
        queries = np.expand_dims(queries, axis=1)

        # if (BATCH_NUM_QUERIES - 1 > 0):
        #    fake_queries = np.zeros((BATCH_NUM_QUERIES - 1, 1, NUM_POINTS, 3))
        #    q = np.vstack((queries, fake_queries))
        # else:
        #    q = queries

        #fake_pos = np.zeros((BATCH_NUM_QUERIES, POSITIVES_PER_QUERY, NUM_POINTS, 3))
        #fake_neg = np.zeros((BATCH_NUM_QUERIES, NEGATIVES_PER_QUERY, NUM_POINTS, 3))
        #fake_other_neg = np.zeros((BATCH_NUM_QUERIES, 1, NUM_POINTS, 3))
        #o1, o2, o3, o4 = run_model(model, q, fake_pos, fake_neg, fake_other_neg)
        with torch.no_grad():
            queries_tensor = torch.from_numpy(queries).float()
            queries_tensor = queries_tensor.to(device)
            o1 = model(queries_tensor)

        output = o1.detach().cpu().numpy()
        output = np.squeeze(output)
        if (q_output.shape[0] != 0):
            q_output = np.vstack((q_output, output))
        else:
            q_output = output

    model.train()
    print(q_output.shape)
    return q_output


def run_model(device, model, queries, positives, negatives, other_neg, require_grad=True):
    """ Computes the forward of the model 
    
    It computes the forward pass of the model. Predict f by passing the given ndarrays of different 
    point clouds to the model.

    Outputs:    
        o1  - descriptor of query PCL, shape=(batch_size, 1, feature_dim)   
        o2  - descriptors of positive PCLs, shape=(batch_size, num_positives_per_query, feature_dim)   
        o3  - descriptors of negative PCLs, shape=(batch_size, num_negatives_per_query, feature_dim)   
        o4  - descriptor of other negative PCL, shape=(batch_size, 1, feature_dim)   
    """
    
    # convert numpy arrays to tensors and concatenate them to one tensor, where
    # each given array as a shape of BATCH_NUM_QUERIES x Number of PCL x NUM_POINTS x 3
    queries_tensor = torch.from_numpy(queries).float()  # the .float() function is not necessary to ensure the data-type is torch.float32 (its inferred from np.float32)
    positives_tensor = torch.from_numpy(positives).float()
    negatives_tensor = torch.from_numpy(negatives).float()
    other_neg_tensor = torch.from_numpy(other_neg).float()

    # feed_tensor: BATCH_NUM_QUERIES x (1 + TRAIN_POSITIVES_PER_QUERY + TRAIN_POSITIVES_PER_QUERY + 1) x NUM_POINTS x NUM_DIM
    feed_tensor = torch.cat((queries_tensor, positives_tensor, negatives_tensor, other_neg_tensor), dim=1)

    # reshape to a new tensor of BATCH_NUM_QUERIES * (1 + TRAIN_POSITIVES_PER_QUERY + TRAIN_POSITIVES_PER_QUERY + 1) x NUM_POINTS x 3 or 13
    num_dim = 13 if MODEL_TYPE == ModelType.LPD_NET else 3
    feed_tensor = feed_tensor.view((-1, 1, NUM_POINTS, num_dim))
    feed_tensor.requires_grad_(require_grad)    # enable autograd to record operations on this tensor
    feed_tensor = feed_tensor.to(device)        # change device type to GPU

    # Forward-Pass
    output = model(feed_tensor)
    output = output.view(BATCH_NUM_QUERIES, -1, FEATURE_OUTPUT_DIM)
    # reshape network output to BATCH_NUM_QUERIES x (1 + TRAIN_POSITIVES_PER_QUERY + TRAIN_POSITIVES_PER_QUERY + 1) x FEATURE_OUTPUT_DIM

    # and split the output tensor into individual feature vectors according to the tuple definion --> BATCH_NUM_QUERIES x Number of PCL x FEATURE_OUTPUT_DIM
    o1, o2, o3, o4 = torch.split(output, [1, TRAIN_POSITIVES_PER_QUERY, TRAIN_NEGATIVES_PER_QUERY, 1], dim=1)
    return o1, o2, o3, o4

def setupAndtrainNetwork():
    """ Verifies environement and loads preprocessed queries """

    # --- verify environment - is CUDA available? --- 
    print ("CUDA-Support: {0} ..version {1}, cuDNN {2} ".format(torch.cuda.is_available(), torch.version.cuda, torch.backends.cudnn.version()))
    if not torch.cuda.is_available():
        print("Training the network without GPU acceleration doesn't work")
        return 0

    # --- verify data/file handling ---
    print("Used source directory: {0}".format(SOURCE_DIR))
    print("Used data directory: {0} with model {1}".format(DATA_DIR, MODEL_FILENAME))
    
    # --- load training/testing queries ---
    global TRAINING_QUERIES, TEST_QUERIES
    idxQueries = 1 if (MODEL_TYPE == ModelType.LPD_NET) else 0
    print ("Loading {0} ...".format(TRAIN_FILE_PKL[idxQueries]))
    TRAINING_QUERIES = util_pcl.get_queries_dict(TRAIN_FILE_PKL[idxQueries])
    print ("Loading {0} ...".format(TEST_FILE_PKL[idxQueries]))
    TEST_QUERIES     = util_pcl.get_queries_dict(TEST_FILE_PKL[idxQueries])
    print("Available queries training: {0}, testing {1}".format(len(TRAINING_QUERIES), len(TEST_QUERIES)))

    # --- log configuration ---
    print("Training configuration:\n\tDATA\tbatch_num_queries={0}, num_pos_per_query={1}, num_neg_per_query={2}, max_epoch={3}\n".format(BATCH_NUM_QUERIES,
    TRAIN_POSITIVES_PER_QUERY, TRAIN_NEGATIVES_PER_QUERY, MAX_EPOCH)) 

    # --- train the network ---
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    torch.set_default_tensor_type(torch.cuda.FloatTensor)

    print("Device: {0}".format(device))

    train(device)

if __name__ == "__main__":
    setupAndtrainNetwork()