#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 16:18:53 2020

@author: Tim Liechti
"""
#------------------------------------------------------------------------------
import os
import numpy as np
import pandas as pd
from numpy import linalg as la

#BASE_DIR = os.path.dirname(os.path.abspath(__file__))
base_path = "../../TESTING/CreateNeighborhood/benchmark_datasets/"
runs_folder="oxford/"
filename = "point_neighborhoods_20m_10overlap.csv"
pointcloud_fols="pointcloud_25m_10/"
neighborhood_fols="KNN_neighborhood_25m_10/"
# -----------------------------------------------------------------------------
#def get_neighborhood():
#	# get neighborhood from file
#	# return it as a nx3 array
#
#	return neighborhood
#------------------------------------------------------------------------------
def get_eigen(neighborhood):
	# neighborhood is a nx3 vector
	# mcovar is a 3x3 covariance matrix
	# print(np.transpose(neighborhood))
	# print(np.transpose(neighborhood).shape)
	mcovar = np.cov(np.transpose(neighborhood),rowvar=False)
	eigenval, eigenvec = la.eig(mcovar)

	return eigenval, eigenvec
#------------------------------------------------------------------------------
def get_entropy(neighborhood):
	# neighborhood needs to be an np.array
	eigenval, eigenvec = get_eigen(neighborhood)
	#
	lam3d1 = eigenval[0]
	lam3d2 = eigenval[1]
	lam3d3 = eigenval[2]

	#
	Sig = lam3d1+lam3d2+lam3d3

	#
	e1 = (lam3d1/Sig)+0.000000001
	e2 = (lam3d2/Sig)+0.000000001
	e3 = (lam3d3/Sig)+0.000000001
	# Shannon entropy
	# entropy =
	# Eigenentropy
	entropy = -e1*np.log(e1)-e2*np.log(e2)-e3*np.log(e3)
	return entropy
#------------------------------------------------------------------------------
def local_feature_extraction(pointcloud): # pointcloud,
	print("INFO: Extracting local features: ...")
	# for every point in the pointcloud
		# load neightborhood index
		# calculate features
		# aggregate features in array
	# pass on featrue array

	# --* needs testing *--
	# list folders in directory
	all_folders=sorted(os.listdir(os.path.join(base_path,runs_folder)))
	features = []
	for folder in all_folders[1:]:  # remove 1 if not running on mac
		df_locations= pd.read_csv(os.path.join(base_path,runs_folder,folder,filename),sep=',')
		df_locations['timestamp']=runs_folder+folder+'/'+pointcloud_fols+df_locations['timestamp'].astype(str)+'.bin'
		df_locations=df_locations.rename(columns={'timestamp':'file'})
		# create neighborhood data folder
		neighborhood_path = os.path.join(base_path, runs_folder, folder, neighborhood_fols)
		os.makedirs(neighborhood_path)

		# list files in folder
		neighborhood_files = df_locations['file'].tolist()
		for file in neighborhood_files:
			fromfilename = file
			# get neighborhood
			neighborhood_data = np.fromfile(fromfilename,dtype=np.float64)

			#
			for line in range(0,np.size(neighborhood_data,0)-1):
				indices = neighborhood_data[line,1:]
				neighborhood = pointcloud(indices)
				k_opt = neighborhood_data[line,0]

				# do
				eigenval, eigenvec = get_eigen(neighborhood)

				# do
				lam3D1 = eigenval[0]
				lam3D2 = eigenval[1]
				lam3D3 = eigenval[2]

				# do
				Sig = lam3D1+lam3D2+lam3D3
				e1 = (lam3D1/Sig)+0.000000001
				e2 = (lam3D2/Sig)+0.000000001
				e3 = (lam3D3/Sig)+0.000000001

				# --- F3D features ---
				# Linearity
				L = (e1-e2)/e1
				# Planarity
				P = (e2-e3)/e1
				# Scattering
				S = e3/e1
				# Change of curvature
				C = e3/(e1+e2+e3)
				# Omnivariance
				O = np.cbrt(e1*e2*e3)
				# Anisotropy
				A = (e1-e3)/e1
				# Eigenentropy
				E = -e1*np.log(e1)-e2*np.log(e2)-e3*np.log(e3)
				# Local point density
				D = k_opt/(4/3*(lam3D1*lam3D2*lam3D3))
				# Sum of eigenvalues
				# Sig

				# --- F2D features ---
				# 2D linearity
				L2D = lam2D1+lam2D2
				# 2D scattering
				S2D = lam2D1/lam2D2

				# --- FV features ---
				# Vertical componenf of normal vector
				vert = 0

				# --- FZ features ---
				# Maximum height difference
				deltaZmax = 0
				# Maximum height variance
				varZmax = 0

				features.np.append([L, P, S, C, O, A, E, D, Sig, L2D, S2D, vert, deltaZmax, varZmax])

	print("done!")
	return features
#------------------------------------------------------------------------------