#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 15:22:27 2020

@author: timsimonliechti
"""
#------------------------------------------------------------------------------
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

base_path = "../../TESTING/CreateNeighborhood/benchmark_datasets/"
runs_folder="oxford/"
filename = "pointcloud_locations_20m_10overlap.csv"
#pointcloud_fols="pointcloud_20m_10overlap/"
neighborhood_fols="KNN_neighborhood_20m_10_logK1000p/" # LOGARITHMIC OPTION -
# LOGARITHMIC OPTION - neighborhood_fols="KNN_neighborhood_20m_10_logK
# LINEAR OPTION - neighborhood_fols="KNN_neighborhood_20m_10/"
#------------------------------------------------------------------------------
def get_neighborhood_stats(min_k, k_log_dist):#min_k, k_log_dist_max
	# LINEAR OPTION - max_k = 100
	#max_k = 503#99 # LOGARITHMIC OPTION -
	k_log_dist_max = k_log_dist[-1]
	k_div = k_log_dist_max + min_k + 1
	all_folders=sorted(os.listdir(os.path.join(base_path,runs_folder)))
	if ".DS_Store" in all_folders:
		all_folders.remove(".DS_Store")
	cloud_values=list()
	for folder in all_folders: # iterating over folders --> scenes
		df_locations=pd.read_csv(os.path.join(base_path,runs_folder,folder,filename),sep=',')
		fromfile_locations=runs_folder+folder+'/'+neighborhood_fols+df_locations['timestamp'].astype(str)+'.bin'
		neighborhood_files = fromfile_locations.tolist()
		point_values=list()
		print("-----------------------")
		for file in neighborhood_files[0:3]: # iterating over files --> point clouds
			fromfilename=os.path.join(base_path,file)
			neighborhood_data = np.fromfile(fromfilename,dtype=np.int,sep=',')
			neighborhood_data = np.reshape(neighborhood_data,(neighborhood_data.shape[0]//k_div,k_div))
			print("-----------------------")
			#print(neighborhood_data)
			#print(neighborhood_data.shape)
			values = neighborhood_data[:,0]
			#print(values)
			point_values.append(values)
			#print(value_list)
		cloud_values.append(point_values)

	value_array = np.asarray(cloud_values)
	#print(value_array.shape)
	#print(value_array)
	data = value_array

	all_data = data.flatten()
	#print(all_data.size)
	plt.hist(all_data, bins=k_log_dist, alpha=0.5)
	#plt.yscale('log')
	plt.xscale('log')

	#k_counts, k_bins = np.histogram(data[0,:,:], bins=k_log_dist)
#	plt.hist(bins[:-1], bins, weights=counts, alpha=0.5)
#	plt.hist(data[0,2,:], bins=k_log_dist, alpha=0.5)
#	plt.yscale('log')

#	counts, bins = np.histogram(data[1,:,:], bins=k_log_dist)
#	plt.hist(bins[:-1], bins, weights=counts, alpha=0.5)
#	plt.yscale('log')
#
#	counts, bins = np.histogram(data[2,:,:], bins=k_log_dist)
#	plt.hist(bins[:-1], bins, weights=counts, alpha=0.5)
#	plt.yscale('log')

#	counts, bins = np.histogram(data[3,:,:])
#	plt.hist(bins[:-1], bins, weights=counts, alpha=0.5)
#	plt.yscale('log')
#
#	counts, bins = np.histogram(data[4,:,
#	plt.hist(bins[:-1], bins, weights=counts, alpha=0.5)
#	plt.yscale('log')
#
#	counts, bins = np.histogram(data[5,:,:])
#	plt.hist(bins[:-1], bins, weights=counts, alpha=0.5)
#	plt.yscale('log')
#
#	counts, bins = np.histogram(data[6,:,:])
#	plt.hist(bins[:-1], bins, weights=counts, alpha=0.5)
#	plt.yscale('log')
#
#	counts, bins = np.histogram(data[7,:,:])
#	plt.hist(bins[:-1], bins, weights=counts, alpha=0.5)
#	plt.yscale('log')
#
#	counts, bins = np.histogram(data[8,:,:])
#	plt.hist(bins[:-1], bins, weights=counts, alpha=0.5)
#	plt.yscale('log')
#
#	counts, bins = np.histogram(data[9,:,:])
#	plt.hist(bins[:-1], bins, weights=counts, alpha=0.5)
#	plt.yscale('log')

#get_neighborhood_stats()

#------------------------------------------------------------------------------
def main():

	min_k = 2
	min_k_log = 0.3
	max_k_log = 2.7
	step_k = 24

	k_log_dist = np.logspace(min_k_log, max_k_log, step_k, endpoint=True, base=10)
	k_log_dist = k_log_dist.astype(int)
	get_neighborhood_stats(min_k, k_log_dist)

#------------------------------------------------------------------------------
if __name__ == '__main__':
	main()