# library/module dependencies
import argparse
import math
import numpy as np
import os
import sys
import torch
import torch.nn as nn
from sklearn.neighbors import NearestNeighbors
from sklearn.neighbors import KDTree
from enum import Enum

# project related modules
import models.PointNetVlad as PNV
import models.LPDNet as LPD
import utils.loading_pointclouds as util_pcl

# network-type and dimensionality defintion
class ModelType(Enum):
    POINTNET_VLAD = 1
    LPD_NET       = 2

MODEL_TYPE = ModelType.POINTNET_VLAD

BATCH_SIZE_EVAL = 5
NUM_POINTS  = 4096
NUM_DIM     = 13 if MODEL_TYPE == ModelType.LPD_NET else 3
OUTPUT_DIM  = 256

# file handling for testing/inference
#SOURCE_DIR  = '/home/src/3DVisionETH/source/'
SOURCE_DIR  = '/home/neur/private/3DVision/source/'
#DATA_DIR    = '/home/data/3DVisionETH/'
DATA_DIR    = '/home/neur/private/3DVision/'

DATASETS = DATA_DIR + 'benchmark_datasets'
EVAL_DATABASE_FILE = [ DATA_DIR + 'queries/oxford_evaluation_database.pickle',
                       DATA_DIR + 'queries/oxford_evaluation_database_LPD.pickle']
EVAL_QUERY_FILE  = [ DATA_DIR + 'queries/oxford_evaluation_query.pickle',
                     DATA_DIR + 'queries/oxford_evaluation_query_LPD.pickle']

MODEL_FILENAME = "model_PNV_bestPos_CodeAdapted.pth.tar"
#MODEL_FILENAME = "model_LPD_firstTry.pth.tar"


def evaluate_model(device):

    # --- define and load trained model ---
    if MODEL_TYPE == ModelType.LPD_NET:
        model = LPD.LPDNet(num_points=NUM_POINTS, output_dim=OUTPUT_DIM, use_neighborhood_aggr=False)
    else:
        model = PNV.PointNetVlad(global_feat=True, feature_transform=True, max_pool=False, output_dim=OUTPUT_DIM, num_points=NUM_POINTS)
    model = model.to(device)    # make sure the model is moved to the GPU and its in the evaluation mode
    model.eval()

    fileToResume = DATA_DIR + MODEL_FILENAME
    print("Load model from ", fileToResume)
    checkpoint = torch.load(fileToResume)
    model.load_state_dict(checkpoint['state_dict'])

    # --- load evaluation data ---
    idxData = 1 if (MODEL_TYPE == ModelType.LPD_NET) else 0
    print ("Loading {0} ...".format(EVAL_DATABASE_FILE[idxData]))
    database_sets = util_pcl.get_sets_dict(EVAL_DATABASE_FILE[idxData])
    print ("Loading {0} ...".format(EVAL_QUERY_FILE[idxData]))
    query_sets    = util_pcl.get_sets_dict(EVAL_QUERY_FILE[idxData])
    print("Available database_sets: {0}, query_sets {1}".format(len(database_sets), len(query_sets)))

    recall = np.zeros(25)
    count = 0
    similarity = []
    one_percent_recall = []

    database_vectors = []
    query_vectors = []

    for i in range(len(database_sets)):
        database_vectors.append(get_latent_vectors(device, model, database_sets[i]))

    for j in range(len(query_sets)):
        query_vectors.append(get_latent_vectors(device, model, query_sets[j]))

    for m in range(len(query_sets)):
        for n in range(len(query_sets)):
            if (m == n):
                continue
            pair_recall, pair_similarity, pair_opr = get_recall(m, n, database_vectors, query_vectors, query_sets)
            recall += np.array(pair_recall)
            count += 1
            one_percent_recall.append(pair_opr)
            for x in pair_similarity:
                similarity.append(x)

    print()
    ave_recall = recall / count
    print(ave_recall)

    # print(similarity)
    average_similarity = np.mean(similarity)
    print(average_similarity)

    ave_one_percent_recall = np.mean(one_percent_recall)
    print(ave_one_percent_recall)

    with open("{0}.txt".format(MODEL_FILENAME[0:-8]), "w") as output:
        output.write("Average Recall @N:\n")
        output.write(str(ave_recall))
        output.write("\n\n")
        output.write("Average Similarity:\n")
        output.write(str(average_similarity))
        output.write("\n\n")
        output.write("Average Top 1% Recall:\n")
        output.write(str(ave_one_percent_recall))
        
    return ave_one_percent_recall


def get_latent_vectors(device, model, dict_to_process):

    file_idxs = np.arange(0, len(dict_to_process.keys()))
    q_output = []
    for q_index in range(len(file_idxs)//BATCH_SIZE_EVAL):
        # iterate through dict_to_process in chunks defined by BATCH_SIZE_EVAL
        file_indices = file_idxs[q_index *BATCH_SIZE_EVAL:(q_index+1)*(BATCH_SIZE_EVAL)]
        file_names   = [ dict_to_process[index]["query"] for index in file_indices]
        queries = util_pcl.load_pc_files(DATASETS,file_names)

        with torch.no_grad():
            feed_tensor = torch.from_numpy(queries).float()
            feed_tensor = feed_tensor.view((-1, 1, NUM_POINTS, NUM_DIM))    # ensure BATCH_SIZE x 1 x POINTS x DIM for the input tensor
            feed_tensor = feed_tensor.to(device)                            # make sure tensor is on the correct device
            out = model(feed_tensor)

        out = out.detach().cpu().numpy()    # a copy of the tensor detached from the graph in cpu memory is returned
        out = np.squeeze(out)
        q_output.append(out)

    q_output = np.array(q_output)
    if(len(q_output) != 0):
        q_output = q_output.reshape(-1, q_output.shape[-1])
    # q_output: num_q_index x BATCH_SIZE_EVAL x NUM_DIM ---> num_q_index*BATCH_SIZE_EVAL x NUM_DIM

    # Edge case(s): If num of files is not a muliple of the chunk size -> make sure those are processed too
    index_edge = len(file_idxs) // BATCH_SIZE_EVAL * BATCH_SIZE_EVAL
    if index_edge < len(dict_to_process.keys()):
        file_indices = file_idxs[index_edge:len(dict_to_process.keys())]
        file_names   = [ dict_to_process[index]["query"] for index in file_indices]
        queries = util_pcl.load_pc_files(DATASETS,file_names)

        with torch.no_grad():
            feed_tensor = torch.from_numpy(queries).float()
            feed_tensor = feed_tensor.view((-1, 1, NUM_POINTS, NUM_DIM))
            feed_tensor = feed_tensor.to(device)
            out = model(feed_tensor)

        out = out.detach().cpu().numpy()
        out = np.squeeze(out)

        if (q_output.shape[0] != 0):
            q_output = np.vstack((q_output, out))
        else:
            q_output = output
    return q_output


def get_recall(m, n, DATABASE_VECTORS, QUERY_VECTORS, QUERY_SETS):
    #database_ouput -> all vectors of submaps of traversal specifief by index m; queries_output --> all test vectors of traversal n
    database_output = DATABASE_VECTORS[m]
    queries_output = QUERY_VECTORS[n]
    print(len(queries_output))
    database_nbrs = KDTree(database_output)

    num_neighbors = 25
    recall = [0] * num_neighbors

    top1_similarity_score = []
    one_percent_retrieved = 0
    threshold = max(int(round(len(database_output)/100.0)), 1)

    num_evaluated = 0
    for i in range(len(queries_output)):
        true_neighbors = QUERY_SETS[n][i][m]
        if(len(true_neighbors) == 0):
            continue
        num_evaluated += 1
        distances, indices = database_nbrs.query(
            np.array([queries_output[i]]),k=num_neighbors)
        for j in range(len(indices[0])):
            if indices[0][j] in true_neighbors:
                if(j == 0):
                    similarity = np.dot(queries_output[i], database_output[indices[0][j]])
                    top1_similarity_score.append(similarity)
                recall[j] += 1
                break

        if len(list(set(indices[0][0:threshold]).intersection(set(true_neighbors)))) > 0:
            one_percent_retrieved += 1

    one_percent_recall = (one_percent_retrieved/float(num_evaluated))*100
    recall = (np.cumsum(recall)/float(num_evaluated))*100
    #print(recall)
    #print(np.mean(top1_similarity_score))
    print(one_percent_recall)
    return recall, top1_similarity_score, one_percent_recall


if __name__ == "__main__":
    
    # --- verify environment - is CUDA available? --- 
    print ("CUDA-Support: {0} ..version {1}, cuDNN {2} ".format(torch.cuda.is_available(), torch.version.cuda, torch.backends.cudnn.version()))
    if not torch.cuda.is_available():
        print("Evaluation of the network will be without GPU acceleration")

    # --- evaluate network ---
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    evaluate_model(device)
