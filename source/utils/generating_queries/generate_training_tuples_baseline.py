import pandas as pd
import numpy as np
import os
import pandas as pd
from sklearn.neighbors import KDTree
import pickle
import random

base_path = "/home/neur/private/3DVision/benchmark_datasets/"
runs_folder= "oxford/"

filename = "pointcloud_locations_20m_10overlap.csv"
pointcloud_fols="/pointcloud_20m_10overlap/"

all_folders=sorted(os.listdir(os.path.join(base_path,runs_folder)))

folders=[]

#All runs are used for training (both full and partial)
index_list=range(len(all_folders)-1)
print("Number of runs: "+str(len(index_list)))
for index in index_list:
	folders.append(all_folders[index])
print(folders)
# [ANALYSIS] ...each folder respresents one traversal in the oxford set; each traversal consists of multiple preprocessed
# submaps, where each submap is a <timestamp>.bin file; in the corresponding .csv file one row equals a submap, where the name of the
# submap is consistent with the first column(timestamp) inside the .csv-file

#####For training and test data split#####
x_width=150
y_width=150
p1=[5735712.768124,620084.402381]
p2=[5735611.299219,620540.270327]
p3=[5735237.358209,620543.094379]
p4=[5734749.303802,619932.693364]   
p=[p1,p2,p3,p4]
# [ANALYSIS] ....each of points defines the center of a (2*x_width) x (2*y_with) square region used as testing subset of submaps
# compare example in figure 10 of the original paper

def check_in_test_set(northing, easting, points, x_width, y_width):
	in_test_set=False
	for point in points:
		if(point[0]-x_width<northing and northing< point[0]+x_width and point[1]-y_width<easting and easting<point[1]+y_width):
			in_test_set=True
			break
	return in_test_set
##########################################


def construct_query_dict(df_centroids, filename):
	tree = KDTree(df_centroids[['northing','easting']])
	ind_nn = tree.query_radius(df_centroids[['northing','easting']],r=10)
	ind_r = tree.query_radius(df_centroids[['northing','easting']], r=50)
	# [ANALYSIS] constructs a KDTree (=multi-dim binary search tree) from all centroids --> queries the indicies of neighours
	# within the radius r; 
	# Hence, the output shape of ind_nn, ind_r = df_centroids.shape[:-1] --> where each element equals an array of
	# indicies for the neighbors of the corresponding point

	queries={}
	for i in range(len(ind_nn)):
		query=df_centroids.iloc[i]["file"]
		positives=np.setdiff1d(ind_nn[i],[i]).tolist()
		negatives=np.setdiff1d(df_centroids.index.values.tolist(),ind_r[i]).tolist()
		random.shuffle(negatives)
		queries[i]={"query":query,"positives":positives,"negatives":negatives}

		# for each submap in df_centroids construct a query with positives defined as all submaps with a centroid within 10m radius
		# and all negatives as the all submaps with min. 50m radius

	with open(filename, 'wb') as handle:
	    pickle.dump(queries, handle, protocol=pickle.HIGHEST_PROTOCOL)

	print("Done ", filename)


####Initialize pandas DataFrame
df_train= pd.DataFrame(columns=['file','northing','easting'])
df_test= pd.DataFrame(columns=['file','northing','easting'])
# [ANALYSIS] ...a Dataframe is data object for 2D, size-mutable and heterogneous tabular data
 
for folder in folders:
	df_locations= pd.read_csv(os.path.join(base_path,runs_folder,folder,filename),sep=',')
	df_locations['timestamp']=runs_folder+folder+pointcloud_fols+df_locations['timestamp'].astype(str)+'.bin'
	df_locations=df_locations.rename(columns={'timestamp':'file'})
	
	# [ANALYSIS] ... the csv-file with GPS coordinates from the subdir is loaded and parsed, where the timestamp column is extended
	# with the subdir/file/timestamp.bin
	print("[dbg] df_locations ..first entry", df_locations.values[0,:])
	print("[dbg] df_locations ..last entry", df_locations.values[0,:])
	print("[dbg] Number of entries", df_locations.values.shape)
	# print(df_locations)

	for index, row in df_locations.iterrows():
		if(check_in_test_set(row['northing'], row['easting'], p, x_width, y_width)):
			df_test=df_test.append(row, ignore_index=True)
		else:
			df_train=df_train.append(row, ignore_index=True)
	
	#print(df_test)
	#print(df_train)
	# [ANALYSIS] ...all available submaps from the 45 traversals defined by 'folders' are iterate -> every submap with centroid coordinates (northing,easting) that are within a region
	# of (2*x_width) x (2*y_width) in meters centered around random chosen points p1 - p4
	
print("Number of training submaps: "+str(len(df_train['file'])))
print("Number of non-disjoint test submaps: "+str(len(df_test['file'])))
construct_query_dict(df_train,"training_queries_baseline.pickle")
construct_query_dict(df_test,"test_queries_baseline.pickle")
# [ANALYSIS] training_queries_baseline.pickle -> 21711 queries with each defined as
# 		{"query": 		pathdir of submap = oxford/traversal/pointcloud_20m_10overlap/<ID>.bin
# 		,"positives": 	array of idx of submaps within 10m of submap_idx,
# 		, "negatives":	array of idx of all submaps outside 50m of submap_idx 
# 		}
# test_queries_baseline.pickle -> 9742 queries of the same format
