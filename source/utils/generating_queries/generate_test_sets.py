import pandas as pd
import numpy as np
import os
import pandas as pd
from sklearn.neighbors import KDTree
import pickle
import random

#####For training and test data split#####
x_width=150
y_width=150

#For Oxford
p1=[5735712.768124,620084.402381]
p2=[5735611.299219,620540.270327]
p3=[5735237.358209,620543.094379]
p4=[5734749.303802,619932.693364]   

#For University Sector
p5=[363621.292362,142864.19756]
p6=[364788.795462,143125.746609]
p7=[363597.507711,144011.414174]

#For Residential Area
p8=[360895.486453,144999.915143]
p9=[362357.024536,144894.825301]
p10=[361368.907155,145209.663042]

p_dict={"oxford":[p1,p2,p3,p4], "university":[p5,p6,p7], "residential": [p8,p9,p10], "business":[]}

def check_in_test_set(northing, easting, points, x_width, y_width):
	in_test_set=False
	for point in points:
		if(point[0]-x_width<northing and northing< point[0]+x_width and point[1]-y_width<easting and easting<point[1]+y_width):
			in_test_set=True
			break
	return in_test_set
##########################################

def output_to_file(output, filename):
	with open(filename, 'wb') as handle:
	    pickle.dump(output, handle, protocol=pickle.HIGHEST_PROTOCOL)
	print("Done ", filename)


def construct_query_and_database_sets(base_path, runs_folder, folders, pointcloud_fols, filename, p, output_name):
	database_trees=[]
	test_trees=[]
	for folder in folders:
		print(' [dbg] process {0}'.format(folder))
		df_database= pd.DataFrame(columns=['file','northing','easting'])
		df_test= pd.DataFrame(columns=['file','northing','easting'])
		df_locations= pd.read_csv(os.path.join(base_path,runs_folder,folder,filename),sep=',')
		print(' [dbg] csv-file {0}'.format(os.path.join(base_path,runs_folder,folder,filename)))
		# df_locations['timestamp']=runs_folder+folder+pointcloud_fols+df_locations['timestamp'].astype(str)+'.bin'
		# df_locations=df_locations.rename(columns={'timestamp':'file'})
		for index, row in df_locations.iterrows():
			#entire business district is in the test set
			if(output_name=="business"):
				df_test=df_test.append(row, ignore_index=True)
			elif(check_in_test_set(row['northing'], row['easting'], p, x_width, y_width)):
				df_test=df_test.append(row, ignore_index=True)
			df_database=df_database.append(row, ignore_index=True)

		print(' [dbg] Number of submaps {0} --> testing: {1}'.format(df_database.shape[0],df_test.shape[0]))
		database_tree = KDTree(df_database[['northing','easting']])
		test_tree = KDTree(df_test[['northing','easting']])
		database_trees.append(database_tree)
		test_trees.append(test_tree)
		# [ANALYSIS] for each csv-file / each traversal a KD-Tree from the centroids of all available submaps (database) and from selected
		# testing are created separatly --> preprocessing step for building the sets!
	print(' [dbg] Created KD-Trees: {0}'.format(len(database_trees)))

	test_sets=[]
	database_sets=[]
	for folder in folders:
		database={}
		test={} 
		df_locations= pd.read_csv(os.path.join(base_path,runs_folder,folder,filename),sep=',')
		df_locations['timestamp']=runs_folder+folder+pointcloud_fols+df_locations['timestamp'].astype(str)+'.bin'
		df_locations=df_locations.rename(columns={'timestamp':'file'})
		for index,row in df_locations.iterrows():				
			#entire business district is in the test set
			if(output_name=="business"):
				test[len(test.keys())]={'query':row['file'],'northing':row['northing'],'easting':row['easting']}
			elif(check_in_test_set(row['northing'], row['easting'], p, x_width, y_width)):
				test[len(test.keys())]={'query':row['file'],'northing':row['northing'],'easting':row['easting']}
			database[len(database.keys())]={'query':row['file'],'northing':row['northing'],'easting':row['easting']}
		database_sets.append(database)
		test_sets.append(test)		
	# [ANALYSIS] Another preprocessing step: Create a dict() for each submap per traversal -> dict of dicts per submap (key=index of submap) that is defined
	# 	{"query": 		pathdir of submap = oxford/traversal/pointcloud_20m/<ID>.bin
	# 	,"northing": 	UTM coordinate of centroid of this submap
	#	, "easting":	UTM coordinate of centroid of this submap
	#	}
	print(' [dbg] Created Sets: {0}'.format(len(database_sets)))
	# [ANALYSIS] At this point there exists for each submap in a traversal a KD-Tree build from UTM coordinates and a dict() as defined
	# above

	for i in range(len(database_sets)):
		tree=database_trees[i]
		for j in range(len(test_sets)):
			if(i==j):
				continue
			for key in range(len(test_sets[j].keys())):
				coor=np.array([[test_sets[j][key]["northing"],test_sets[j][key]["easting"]]])
				index = tree.query_radius(coor, r=25)
				#indices of the positive matches in database i of each query (key) in test set j
				test_sets[j][key][i]=index[0].tolist()
				#print(test_sets[j][key])
			

	# [ANALYSIS]:
	# database_sets = list<dict<dict>>, where each entry of the list represents one considered traversal as a dict
	# with dict[index] = {'query': file, 'northing': value, 'easting': value} (see above) and index is just the numbering of the submap inside the traversal
	# test_sets = list<dict<dicts>> which has the same structure as the database_sets, but only includes the number of considered
	# submaps that are within x_with/y_with of the points p1-p4; further the structure of one dict is as follows
	# { 'query': file, 'northing: value, 'easting': values,
	#   0: list_idx, 1: list_idx, i: list_idx, ... , len(database_sets): list_idx  --> for all traversals except the one this query belongs too a list of idx with similar submaps
	# }																				( within 25m ) exists.
	output_to_file(database_sets, output_name+'_evaluation_database.pickle')
	output_to_file(test_sets, output_name+'_evaluation_query.pickle')

###Building database and query files for evaluation
base_path = "/home/neur/private/3DVision/benchmark_datasets/"
oxfordDataOnly = True

#For Oxford
folders=[]
runs_folder= "oxford/"
all_folders=sorted(os.listdir(os.path.join(base_path,runs_folder)))
index_list=[5,6,7,9,10,11,12,13,14,15,16,17,18,19,22,24,31,32,33,38,39,43,44]
if all_folders[0] == '.directory':
	all_folders = all_folders[1:]
print(len(index_list))
for index in index_list:
	folders.append(all_folders[index])
# [ANALYSIS] ...as for training each folder respresents one traversal --> a predefined selected list (index_list) of full traversals
# includes the pointcould_20m.csv defining the submaps for evaluation/inference. As with training each submap stored as <timestamp>.bin
# file corresponds to one row in the .csv file

print(folders)
construct_query_and_database_sets(base_path, runs_folder, folders, "/pointcloud_20m/", "pointcloud_locations_20m.csv", p_dict["oxford"], "oxford")

if oxfordDataOnly:
	exit(0)

#For University Sector
folders=[]
runs_folder = "inhouse_datasets/"
all_folders=sorted(os.listdir(os.path.join(BASE_DIR,base_path,runs_folder)))
uni_index=range(10,15)
for index in uni_index:
	folders.append(all_folders[index])

print(folders)
construct_query_and_database_sets(base_path, runs_folder, folders, "/pointcloud_25m_25/", "pointcloud_centroids_25.csv", p_dict["university"], "university")

#For Residential Area
folders=[]
runs_folder = "inhouse_datasets/"
all_folders=sorted(os.listdir(os.path.join(BASE_DIR,base_path,runs_folder)))
res_index=range(5,10)
for index in res_index:
	folders.append(all_folders[index])

print(folders)
construct_query_and_database_sets(base_path, runs_folder, folders, "/pointcloud_25m_25/", "pointcloud_centroids_25.csv", p_dict["residential"], "residential")

#For Business District
folders=[]
runs_folder = "inhouse_datasets/"
all_folders=sorted(os.listdir(os.path.join(BASE_DIR,base_path,runs_folder)))
bus_index=range(5)
for index in bus_index:
	folders.append(all_folders[index])

print(folders)
construct_query_and_database_sets(base_path, runs_folder, folders, "/pointcloud_25m_25/", "pointcloud_centroids_25.csv", p_dict["business"], "business")
