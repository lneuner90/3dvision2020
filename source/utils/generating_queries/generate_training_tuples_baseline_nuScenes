import pandas as pd
import numpy as np
import os
from sklearn.neighbors import KDTree
import pickle
import random


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
base_path = "../"

runs_folder= "boston_seaport/"
filename = "globalCentroids_boston_seaport.csv"
pointcloud_fols="/featurecloud_20m_10overlap/"

all_folders=sorted(os.listdir(os.path.join(BASE_DIR,base_path,runs_folder)))

folders=[]

#All runs are used for training (both full and partial)
index_list=range(len(all_folders)-1)
print("Number of runs: "+str(len(index_list)))
for index in index_list:
	folders.append(all_folders[index])
print(folders)

#####For training and test data split#####
x_width=225
y_width=225
#Points 1-4 are seed coordinates, indicating  different locations in Boston-Seaport

p1=[2234.599674588268,857.7621268548945]
p2=[1050.3967090680696,1418.0477970782824]
p3=[308.0468837935518,694.3400629320522]
p4=[873.6664383933902,570.6978819524828]   
p=[p1,p2,p3, p4]

def check_in_test_set(northing, easting, points, x_width, y_width):
	in_test_set=False
	for point in points:
		if(point[0]-x_width<northing and northing< point[0]+x_width and point[1]-y_width<easting and easting<point[1]+y_width):
			in_test_set=True
			break
	return in_test_set
##########################################


def construct_query_dict(df_centroids, filename):
	tree = KDTree(df_centroids[['northing','easting']])
	ind_nn = tree.query_radius(df_centroids[['northing','easting']],r=10)
	ind_r = tree.query_radius(df_centroids[['northing','easting']], r=50)
	queries={}
	for i in range(len(ind_nn)):
		query=df_centroids.iloc[i]["file"]
		positives=np.setdiff1d(ind_nn[i],[i]).tolist()
		negatives=np.setdiff1d(df_centroids.index.values.tolist(),ind_r[i]).tolist()
		random.shuffle(negatives)
		queries[i]={"query":query,"positives":positives,"negatives":negatives}

	with open(filename, 'wb') as handle:
	    pickle.dump(queries, handle, protocol=pickle.HIGHEST_PROTOCOL)

	print("Done ", filename)


####Initialize pandas DataFrame
df_train= pd.DataFrame(columns=['file','northing','easting'])
df_test= pd.DataFrame(columns=['file','northing','easting'])

for folder in folders:
	df_locations= pd.read_csv(os.path.join(base_path,runs_folder,folder,filename),sep=',')
	df_locations['timestamp']=runs_folder+folder+pointcloud_fols+df_locations['timestamp'].astype(str)+'.bin'
	df_locations=df_locations.rename(columns={'timestamp':'file'})
	
	for index, row in df_locations.iterrows():
		if(check_in_test_set(row['northing'], row['easting'], p, x_width, y_width)):
			df_test=df_test.append(row, ignore_index=True)
		else:
			df_train=df_train.append(row, ignore_index=True)

print("Number of training submaps: "+str(len(df_train['file'])))
print("Number of non-disjoint test submaps: "+str(len(df_test['file'])))
construct_query_dict(df_train,"training_queries_baseline.pickle")
construct_query_dict(df_test,"test_queries_baseline.pickle")