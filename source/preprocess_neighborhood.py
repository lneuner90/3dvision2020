#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 08:53:52 2020

@author: timsimonliechti
"""
# --- INFO ---
# adjust base_path if necessary
# for linear spacing of k, uncomment LINEAR OPTION
#------------------------------------------------------------------------------
import os
import numpy as np
import pandas as pd
from sklearn.neighbors import NearestNeighbors
from feature_extract import get_entropy
from get_neighborhood_statistics import get_neighborhood_stats

base_path = "../../TESTING/CreateNeighborhood/benchmark_datasets/"
runs_folder="oxford/"
filename = "pointcloud_locations_20m_10overlap.csv"
pointcloud_fols="pointcloud_20m_10overlap/"
neighborhood_fols="KNN_neighborhood_20m_10_logK1000p/" # LOGARITHMIC OPTION -
# LINEAR OPTION - neighborhood_fols="KNN_neighborhood_20m_10/"
#------------------------------------------------------------------------------
def get_opt_neighborhood(pointcloud):
		# iterate through pointcloud
		# for every point do
			# knn with different k
			# pass point cloud to get_entropy to calculate entropy
			# pick k with lowest entropy
			# write point id/index to file

	# pointcloud has to be np.array() from here on
	min_k = 2#20 # LINEAR OPTION -
	# LINEAR OPTION - max_k = 100
	min_k_log = 0.3 # LOGARITHMIC OPTION -
	max_k_log = 2.7#1.9 # LOGARITHMIC OPTION -
	step_k = 24#16
	# LINEAR OPTION - delta_k = int((max_k - min_k)/step_k)
	data_vec = list()
	k_log_dist = np.logspace(min_k_log, max_k_log, step_k, endpoint=True, base=10) # LOGARITHMIC OPTION -
	k_log_dist = k_log_dist.astype(int)
	for line in range(0,1000): # np.size(pointcloud,0)-1
		point = [pointcloud[line,:]]
		point = np.asarray(point)

		#neighborcloud = np.delete(pointcloud,line,0)
		#print("---------------------")
		#print("INFO: Current point is: {}".format(point))
		#indvec = np.zeros((max_k,),dtype=int)
		entropy = list()
		ind_vec = list()
		for steps in range(1, step_k):
			# calculating point neighborhood for different neighborhood sizes
			# LINEAR OPTION - k = min_k + (steps-1)*delta_k
			k = min_k + k_log_dist[steps-1] # LOGARITHMIC OPTION -
			neighbors = NearestNeighbors(n_neighbors=k)
			neighbors.fit(pointcloud)
			distance, index = neighbors.kneighbors(point)

			# LINEAR OPTION - fill = np.zeros((1,max_k-k),dtype=int)
			fill = np.zeros((1,min_k+k_log_dist[-1]-k),dtype=int) # LOGARITHMIC OPTION -

			index = np.concatenate((index, fill), axis=None)

			ind_vec.append(index)

			# ??? delete distance to self
			# write entropy to array
			neighbor_points = pointcloud[index[0:k].astype(int)]
			#neighbor_points = pointcloud[index[0,0:k].astype(int)]
			selected_points = np.reshape(neighbor_points,(k,3))
			entropy_value = get_entropy(selected_points)
			entropy.append(entropy_value)
		# ---- delete first row of indvec

		ind_v = np.asarray(ind_vec)

		# find min entropy
		entropy = np.asarray(entropy)
		# index of the lowest entropy value
		k_ind = np.argmin(entropy)

		# number of neighbors that minimizes the entropy -- Error here?
		# LINEAR OPTION - k_opt = min_k + (k_ind + 1)*delta_k
		k_opt = min_k + k_log_dist[k_ind] # LOGARITHMIC OPTION -
		# indices of neighboring points
		opt_ind = ind_v[k_ind,:]

		line_vec = np.concatenate((k_opt, opt_ind), axis=None)

		data_vec.append(line_vec)

		#print("{}---------------------".format(line+1))
		#print("INFO: Data line (linevec) is: \n{}".format(linevec))
		#print("INFO: Optimum neighborhood size is: {}".format(k_opt))
		#print("INFO: Neighboring points are: \n{}".format(opt_ind[0:k]))
	data_vec = np.asarray(data_vec)
	#data_vec.tofile("data_vec.txt",sep=',')
	#print(data_vec)
	return data_vec
#------------------------------------------------------------------------------
def process_pointclouds():
	# for every file in folder *path
		# load point cloud
		# pass it to get_opt_neighborhood
		# write neighborhood data to new file

	# list folders in directory
	all_folders=sorted(os.listdir(os.path.join(base_path,runs_folder)))
	if ".DS_Store" in all_folders:
		all_folders.remove(".DS_Store")
	for folder in all_folders:
		print("------------------------------------------")
		print("INFO: Processing folder: {} ...".format(folder))
		df_locations= pd.read_csv(os.path.join(base_path,runs_folder,folder,filename),sep=',')
		tofile_locations=runs_folder+folder+'/'+neighborhood_fols+df_locations['timestamp'].astype(str)+'.bin'
		df_locations['timestamp']=runs_folder+folder+'/'+pointcloud_fols+df_locations['timestamp'].astype(str)+'.bin'
		df_locations=df_locations.rename(columns={'timestamp':'file'})
		# create neighborhood data folder
		neighborhood_path = os.path.join(base_path, runs_folder, folder, neighborhood_fols)
		if not os.path.exists(neighborhood_path):
			   os.mkdir(neighborhood_path)
		# list files in folder
		pointcloud_files = df_locations['file'].tolist()
		neighborhood_files = tofile_locations.tolist()
		it = 0
		for file in pointcloud_files[0:3]: #pointcloud_files
			print("INFO:   Processing file: {}.bin".format(file[-20:-4]))
			fromfilename = os.path.join(base_path,file)
			# get pointcloud
			pointcloud = np.fromfile(fromfilename,dtype=np.float64)
			pointcloud = np.reshape(pointcloud,(pointcloud.shape[0]//3,3))
			# get optimal neighborhood and its size
			neighborhood_data = get_opt_neighborhood(pointcloud)
			# write neighborhood data to file in newly created folder
			tofilename = neighborhood_files[it]
			#print(neighborhood_data)
			neighborhood_data.tofile(os.path.join(base_path,tofilename),sep=',')
			it = it+1
		print("done!")
#------------------------------------------------------------------------------
def main():

	process_pointclouds()
	get_neighborhood_stats(min_k, k_log_dist[-1]):

#------------------------------------------------------------------------------
if __name__ == '__main__':
	main()
#------------------------------------------------------------------------------
# find . -name ".DS_Store" -delete